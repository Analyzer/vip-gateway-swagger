package com.example.orderboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 订单服务
 *
 * @author wgb
 * @date 2022/02/16
 */
@SpringBootApplication
@EnableDiscoveryClient
public class OrderBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrderBootApplication.class, args);
	}

}
