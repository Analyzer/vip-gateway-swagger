package com.example.orderboot.model;

import lombok.Data;

/**
 * 响应实体类
 *
 * @author wgb
 * @date 2022/02/16
 */
@Data
public class ResultMsg<T> {
    private Integer code;

    private String msg;

    private T data;
}
