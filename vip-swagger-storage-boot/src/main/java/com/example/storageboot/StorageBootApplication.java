package com.example.storageboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 库存服务
 *
 * @author wgb
 * @date 2022/02/16
 */
@SpringBootApplication
@EnableDiscoveryClient
public class StorageBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(StorageBootApplication.class, args);
	}

}
